package JDBC_Part2.service;

import JDBC_Part2.dao.DAOClientPerson;
import JDBC_Part2.dao.DAOCredit;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Credit;

public class CreditService {
    DAOCredit daoCredit = new DAOCredit();
    DAOClientPerson daoClientPerson = new DAOClientPerson();

    public Credit findCreditById(long id){
        Credit credit = daoCredit.findCreditById(id);
        return credit;
    }

    public boolean createCredit(Credit credit, long id_client){
        ClientPerson person = daoClientPerson.findPersonById(id_client);
        if(daoCredit.createCredit(credit, id_client, person.getBank_account())){
            return true;
        }
        return false;
    }

    public boolean deleteCredit(long id){
        if(daoCredit.deleteCredit(id)){
            return true;
        }
        return false;
    }

    public boolean updateCredit(long id, Credit credit){
        if(daoCredit.updateCredit(id, credit)){
            return true;
        }
        return false;
    }
}
