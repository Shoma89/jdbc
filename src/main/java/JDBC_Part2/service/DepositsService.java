package JDBC_Part2.service;

import JDBC_Part2.dao.DAOClientPerson;
import JDBC_Part2.dao.DAODeposits;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Deposits;

public class DepositsService {
    DAODeposits daoDeposits = new DAODeposits();
    DAOClientPerson daoClientPerson = new DAOClientPerson();

    public Deposits findDepositById(long id){
        Deposits deposits = daoDeposits.findDepositById(id);
        return deposits;
    }
    public boolean createDeposit(Deposits deposits, long id_client){
        ClientPerson person = daoClientPerson.findPersonById(id_client);
        if(daoDeposits.createDeposits(deposits, person.getId(), person.getBank_account())){
            return true;
        }
        return false;
    }

    public boolean deleteDeposits(long id){
        if(daoDeposits.deleteDeposit(id)){
            return true;
        }
        return false;
    }

    public boolean updateDeposit(long id, Deposits deposits){
        if(daoDeposits.updateDeposit(id, deposits)){
            return true;
        }
        return false;
    }
}
