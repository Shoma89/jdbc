package JDBC_Part2.service;

import JDBC_Part2.dao.DAOClientPerson;
import JDBC_Part2.dao.DAOCredit;
import JDBC_Part2.dao.DAODeposits;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Credit;
import JDBC_Part2.model.Deposits;

public class ClientPersonService {
    DAOClientPerson daoClientPerson = new DAOClientPerson();
    DAOCredit daoCredit = new DAOCredit();

    DAODeposits daoDeposits = new DAODeposits();
    ClientPerson clientPerson = new ClientPerson();

    public ClientPerson findPersonById(int id){
        return  clientPerson = daoClientPerson.findPersonById(id);
    }

    public boolean removePerson(long id){
        if(daoClientPerson.deletePerson(id)){
            return true;
        }
        return false;
   }

    public boolean personUpdate(long id, ClientPerson person){
        if(daoClientPerson.updatePerson(id, person)){
            return true;
        }
        return false;
    }

    public boolean makingALoan(ClientPerson person, Credit credit){
        person = daoClientPerson.equalsPerson(person);
        if(daoCredit.createCredit(credit, person.getId(), person.getBank_account())){
            return true;
        }
        return false;
    }

    public boolean openADeposit(ClientPerson person, Deposits deposits){
        person = daoClientPerson.equalsPerson(person);
        if(daoDeposits.createDeposits(deposits, person.getId(), person.getBank_account())){
            return true;
        }
        return false;
    }
}
