package JDBC_Part2.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DBConnect {
    /**
     * @DBConnect - является сингтоном(класс, у которого может быть только один объект)
     * @createConnect - метод для создания подключения к БД, через properties файл
    */
    private static Connection connection;

    public static Connection createConnect(){
        if(connection == null){
            Properties properties = new Properties();
            try(FileInputStream fis = new FileInputStream("src/main/resources/db.properties")) {
                properties.load(fis);
                return DriverManager.getConnection(
                        properties.getProperty("db.url"),
                        properties.getProperty("db.user"),
                        properties.getProperty("db.password")
                );
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return connection;
    }
}
