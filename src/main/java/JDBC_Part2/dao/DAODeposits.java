package JDBC_Part2.dao;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Deposits;
import JDBC_Part2.util.MapperDeposits;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @DAODeposits - класс для работы с БД и объектами класса Deposits
 * @findDepositById - метод ищет депозит в БД по id, и возвращает в виде объекта класса Deposits
 * @createDeposits - метод для добавления депозита в БД и возвращает тип boolean
 * @deleteDeposit - метод удаляет депозит из БД
 * @updateDeposit - метод для изменения условий депозита
 */

public class DAODeposits {

    DAOClientPerson daoPerson = new DAOClientPerson();

    public Deposits findDepositById(long id){
        String databaseQuery = "SELECT * FROM deposits WHERE id = ";
        MapperDeposits mapperDeposits = new MapperDeposits();
        Deposits deposits = null;
        try(PreparedStatement preparedStatement = DBConnect.createConnect().prepareStatement(databaseQuery+ id)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            deposits = mapperDeposits.createDeposit(resultSet);
            return deposits;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return deposits;
    }
    public boolean createDeposits(Deposits deposits, long id_client, long bank_account){
        boolean isSuccess = false;
        String depositInsertInto = ("INSERT INTO " +
                "deposits(id_client, type_deposits, bank_account, deposit_term, deposit_amount, interest_rate) " +
                "VALUES ('" + id_client + "', '" +
                deposits.getType_deposits() + "', '" +
                bank_account + "', '" +
                deposits.getDeposit_term() + "', '" +
                deposits.getDeposit_amount() + "','" +
                deposits.getInterest_rate() + "')"
        );
        try(Statement statement = DBConnect.createConnect().createStatement()) {
            isSuccess = statement.execute(depositInsertInto);
            return isSuccess;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isSuccess;
    }

    public boolean deleteDeposit(long id){
        boolean isSuccess = false;
        String sqlDeleteDeposit = "DELETE FROM deposits WHERE id = " + id;
        try(Statement statement = DBConnect.createConnect().createStatement()){
            Deposits deposits = findDepositById(id);
            ClientPerson person = daoPerson.findPersonById(deposits.getId_client());
            if(deposits.getId() != 0){
                statement.executeUpdate(sqlDeleteDeposit);
                System.out.println("Клиент: " + person);
                System.out.println("Депозит "  + deposits + " был закрыт!");
                isSuccess = true;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " deleteDeposit");
        }
        return isSuccess;
    }

    public boolean updateDeposit(long id, Deposits deposits){
        boolean isSuccess = false;
        Deposits modifyDeposits = findDepositById(id);
        String sqlUpdateDeposit = "UPDATE deposits SET type_deposits = '" +
                deposits.getType_deposits() + "', bank_account = " +
                deposits.getBank_account() + ", deposit_term = " +
                deposits.getDeposit_term() + ", deposit_amount = " +
                deposits.getDeposit_amount() + ", interest_rate = " +
                deposits.getInterest_rate() + " WHERE id = " + id;

        try(Statement statement = DBConnect.createConnect().createStatement()) {
            if(modifyDeposits.getId() != 0){
                statement.executeUpdate(sqlUpdateDeposit);
                System.out.println("Депозит " + modifyDeposits + " был изменён!");
                isSuccess = true;
            } else {
                System.out.println("Депозит с id " + id + " не найден!");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " updateDeposit");
        }
        return isSuccess;
    }
}
