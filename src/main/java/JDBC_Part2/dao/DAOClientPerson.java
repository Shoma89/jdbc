package JDBC_Part2.dao;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.util.MapperClientPerson;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @DAOClientPerson - класс для работы с БД и объектами класса ClientPerson
 * @findPersonById - метод ищет клиента в БД по id, и возвращает в виде объекта класса ClientPerson
 * @createPersonBD - метод для добавления клиента в БД и возвращает тип boolean
 * @equalsPerson - метод для сравнения клиентов в БД и объекта класса ClientPerson
 * @deletePerson - метод для удаления клиента из БД
 * @updatePerson - метод для изменения данных о клиенте в БД!
 */

public class DAOClientPerson {
    private static String databaseQuery = "SELECT * FROM clientperson";
    MapperClientPerson mapperClientPerson = new MapperClientPerson();
    public ClientPerson findPersonById(long id){ //ищем пользователя в БД, и возвращаем его
        ClientPerson clientPerson = null;
        try(PreparedStatement preparedStatement = DBConnect.createConnect().prepareStatement(databaseQuery + " WHERE id = " +id)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            clientPerson = mapperClientPerson.createPerson(resultSet);
            return clientPerson;
        } catch (SQLException e){
            System.out.println(e.getMessage() + " findPersonById");
        }
        return clientPerson;
    }

    public boolean createPersonBD(ClientPerson person){
        boolean isSuccess = false;
        String clientInsertInto = String.format(
                "INSERT INTO clientperson " + "(name, last_name, phone, birth_date) VALUES ('%s', '%s', '%d', '%s')",
                person.getName(), person.getLast_name(), person.getPhone(), person.getBirth_date());

            try (Statement statement = DBConnect.createConnect().createStatement()) {
                isSuccess = statement.execute(clientInsertInto);
                return isSuccess;
            } catch (SQLException e) {
                System.out.println(e.getMessage() + " createPersonBD");
            }
        return isSuccess;
    }

    public ClientPerson equalsPerson(ClientPerson person){

        String fullParamPerson = "SELECT * FROM clientperson WHERE id = (SELECT MAX(id) FROM clientperson)";

        try(PreparedStatement preparedStatement = DBConnect.createConnect().prepareStatement(databaseQuery)){
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                if(person.getName().equals(resultSet.getString("name")) &&
                        person.getLast_name().equals(resultSet.getString("last_name")) &&
                        person.getBirth_date().equals(resultSet.getString("birth_date"))){
                    person = findPersonById(resultSet.getInt("id"));
                    System.out.println("Пользователь уже существует. id: " + resultSet.getInt("id"));
                    return person;
                }
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " equalsPerson");
        }
        createPersonBD(person);
        try(PreparedStatement preparedStatement = DBConnect.createConnect().prepareStatement(fullParamPerson)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            person = mapperClientPerson.createPerson(resultSet);
        } catch (SQLException e){
            System.out.println(e.getMessage() + " equalsPerson 2");
        }
        return person;
    }

    public boolean deletePerson(long id){
        boolean isDelPerson = false;
        String delPersonFromDB = "DELETE FROM clientperson WHERE id = " + id;
        try(Statement statement = DBConnect.createConnect().createStatement()) {
            ClientPerson person = findPersonById(id);
            if(person.getId() != 0L && person.getName() != null) {
                statement.executeUpdate(delPersonFromDB);
                System.out.println("Клиент " + person + " был удалён из БД!");
                isDelPerson = true;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isDelPerson;
    }

    public boolean updatePerson(long id, ClientPerson person){
        boolean isUpPerson = false;
        ClientPerson modifyPerson = findPersonById(id);
        String sqlUpdatePeron = "UPDATE clientperson SET name = '" +
                person.getName() + "', last_name = '" +
                person.getLast_name() + "', phone = " +
                person.getPhone() + ", birth_date = '" +
                person.getBirth_date() + "', bank_account = " +
                person.getBank_account() + " WHERE id = " + id;
        try(Statement statement = DBConnect.createConnect().createStatement()) {
            if(modifyPerson.getId() != 0L && modifyPerson.getName() != null) {
                statement.executeUpdate(sqlUpdatePeron);
                System.out.println("Клиент " + modifyPerson + " был изменён!");
                isUpPerson = true;
            } else {
                System.out.println("Клиент с id " + id + " не найден!");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " updatePerson");
        }
        return isUpPerson;
    }
}
