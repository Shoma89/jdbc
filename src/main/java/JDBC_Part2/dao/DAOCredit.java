package JDBC_Part2.dao;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Credit;
import JDBC_Part2.util.MapperCredit;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @DAOCredit - класс для работы с БД и объектами класса Credit
 * @findCreditById - метод ищет кредит в БД по id, и возвращает в виде объекта класса Credit
 * @createCredit - метод для добавления кредита в БД и возвращает тип boolean
 * @deleteCredit - метод для удаления кредита из БД
 * @updateCredit - метод для изменения условий кредита.
 */

public class DAOCredit {

    DAOClientPerson daoPerson = new DAOClientPerson();
    public Credit findCreditById(long id){
        String databaseQuery = "SELECT * FROM credit WHERE id = ";
        MapperCredit mapperCredit = new MapperCredit();
        Credit credit = null;
        try(PreparedStatement preparedStatement = DBConnect.createConnect().prepareStatement(databaseQuery+ id)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            credit = mapperCredit.createCredit(resultSet);
            return credit;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return credit;
    }

    public boolean createCredit(Credit credit, long id_client, long bank_account){
        boolean isSuccess = false;
        String creditInsertInto = ("INSERT INTO " +
                "credit(id_client, type_credit, bank_account, credit_term, credit_amount, interest_rate) " +
                "VALUES ('" + id_client + "', '" +
                credit.getType_credit() + "', '" +
                bank_account + "', '" +
                credit.getCredit_term() + "', '" +
                credit.getCredit_amount() + "','" +
                credit.getInterest_rate() + "')"
        );
        try(Statement statement = DBConnect.createConnect().createStatement()) {
            isSuccess = statement.execute(creditInsertInto);
            return isSuccess;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isSuccess;
    }

    public boolean deleteCredit(long id){
        boolean isDelCredit = false;
        String sqlDeleteCredit = "DELETE FROM credit WHERE id =" + id;
        try(Statement statement = DBConnect.createConnect().createStatement()) {
            Credit credit = findCreditById(id);
            ClientPerson person = daoPerson.findPersonById(credit.getId_client());
            if(credit.getId() != 0){
                statement.executeUpdate(sqlDeleteCredit);
                System.out.println("Клиент: " + person);
                System.out.println("Кредит " + credit + " был закрыт!");
                isDelCredit = true;
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " deleteCredit");
        }
        return isDelCredit;
    }

    public boolean updateCredit(long id, Credit credit){
        boolean isUpdateCredit = false;
        Credit modifyCredit = findCreditById(id);
        String sqlUpdateCredit = "UPDATE credit SET type_credit = '" +
                credit.getType_credit() + "', bank_account = " +
                credit.getBank_account() + ", credit_term = " +
                credit.getCredit_term() + ", credit_amount = " +
                credit.getCredit_amount() + ", interest_rate = " +
                credit.getInterest_rate() + " WHERE id = " +id;

        try(Statement statement = DBConnect.createConnect().createStatement()){
            if(modifyCredit.getId() != 0){
                statement.executeUpdate(sqlUpdateCredit);
                System.out.println("Кредит " + modifyCredit + " был изменён!");
                isUpdateCredit = true;
            } else {
                System.out.println("Кредит с id " + id + " не найден!");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage() + " updateCredit");
        }
        return isUpdateCredit;
    }
}
