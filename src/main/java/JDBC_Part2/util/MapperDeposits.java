package JDBC_Part2.util;

import JDBC_Part2.model.Deposits;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @createDeposit - конвертирует результат SQL запроса в объект класса MapperDeposits
 */

public class MapperDeposits {
    public Deposits createDeposit(ResultSet resultSet){
        Deposits deposits = new Deposits();
        try {
            deposits.setId(resultSet.getInt("id"));
            deposits.setId_client(resultSet.getInt("id_client"));
            deposits.setType_deposits(resultSet.getString("type_deposits"));
            deposits.setBank_account(resultSet.getInt("bank_account"));
            deposits.setDeposit_term(resultSet.getInt("deposit_term"));
            deposits.setDeposit_amount(resultSet.getInt("deposit_amount"));
            deposits.setInterest_rate(resultSet.getInt("interest_rate"));

            return deposits;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return deposits;
    }
}
