package JDBC_Part2.util;

import JDBC_Part2.model.Credit;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @createCredit - конвертирует результат SQL запроса в объект класса MapperCredit
 */

public class MapperCredit {
    public Credit createCredit(ResultSet resultSet){
        Credit credit = new Credit();
        try {
            credit.setId(resultSet.getInt("id"));
            credit.setId_client(resultSet.getInt("id_client"));
            credit.setType_credit(resultSet.getString("type_credit"));
            credit.setBank_account(resultSet.getInt("bank_account"));
            credit.setCredit_term(resultSet.getInt("credit_term"));
            credit.setCredit_amount(resultSet.getInt("credit_amount"));
            credit.setInterest_rate(resultSet.getInt("interest_rate"));
            return credit;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return credit;
    }
}
