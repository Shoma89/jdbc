package JDBC_Part2.util;

import JDBC_Part2.model.ClientPerson;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @MapperClientPerson - конвертирует результат SQL запроса в объект класса ClientPerson
 */
public class MapperClientPerson {
    public ClientPerson createPerson(ResultSet resultSet){
        ClientPerson clientPerson = new ClientPerson();
        try {
            clientPerson.setId(resultSet.getLong("id"));
            clientPerson.setName(resultSet.getString("name"));
            clientPerson.setLast_name(resultSet.getString("last_name"));
            clientPerson.setPhone(resultSet.getLong("phone"));
            clientPerson.setBirth_date(resultSet.getString("birth_date"));
            clientPerson.setBank_account(resultSet.getLong("bank_account"));

            return clientPerson;
        } catch (SQLException e){
            System.out.println(e.getMessage() + " createPerson MapperClientPerson");
        }
        return clientPerson;
    }
}
