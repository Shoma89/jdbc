package JDBC_Part2.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class Deposits {
    private long id;
    private long id_client;         //id клиента
    private String type_deposits;   //вид вклада
    private long bank_account;      //номер счёта
    private int deposit_term;       //срок вклада
    private long deposit_amount;    //сумма вклада
    private int interest_rate;      //процентная ставка по вкладу

    public Deposits(long id_client, String type_deposits, long bank_account, int deposit_term, long deposit_amount, int interest_rate) {
        this.id_client = id_client;
        this.type_deposits = type_deposits;
        this.bank_account = bank_account;
        this.deposit_term = deposit_term;
        this.deposit_amount = deposit_amount;
        this.interest_rate = interest_rate;
    }

    public Deposits(long id_client, String type_deposits, int deposit_term, long deposit_amount, int interest_rate) {
        this.id_client = id_client;
        this.type_deposits = type_deposits;
        this.deposit_term = deposit_term;
        this.deposit_amount = deposit_amount;
        this.interest_rate = interest_rate;
    }

    public Deposits(String type_deposits, int deposit_term, long deposit_amount, int interest_rate) {
        this.type_deposits = type_deposits;
        this.deposit_term = deposit_term;
        this.deposit_amount = deposit_amount;
        this.interest_rate = interest_rate;
    }
}
