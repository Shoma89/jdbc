package JDBC_Part2.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class Credit {
    private long id;
    private long id_client;         //id клиента
    private String type_credit;     //вид кредита
    private long bank_account;      //номер счёта
    private int credit_term;        //срок кредита
    private long credit_amount;     //сумма кредита
    private int interest_rate;      //процентная ставка кредита

    public Credit(long id_client, String type_credit, long bank_account, int credit_term, long credit_amount, int interest_rate) {
        this.id_client = id_client;
        this.type_credit = type_credit;
        this.bank_account = bank_account;
        this.credit_term = credit_term;
        this.credit_amount = credit_amount;
        this.interest_rate = interest_rate;
    }

    public Credit(String type_credit, long bank_account, int credit_term, long credit_amount, int interest_rate) {
        this.type_credit = type_credit;
        this.bank_account = bank_account;
        this.credit_term = credit_term;
        this.credit_amount = credit_amount;
        this.interest_rate = interest_rate;
    }

    public Credit(String type_credit, int credit_term, long credit_amount, int interest_rate) {
        this.type_credit = type_credit;
        this.credit_term = credit_term;
        this.credit_amount = credit_amount;
        this.interest_rate = interest_rate;
    }
}
