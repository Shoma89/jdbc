package JDBC_Part2.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class ClientPerson{
    private long id;
    private String name;
    private String last_name;
    private long phone;
    private String birth_date;
    private long bank_account;

    public ClientPerson(String name, String last_name, long phone, String birth_date) {
        this.name = name;
        this.last_name = last_name;
        this.phone = phone;
        this.birth_date = birth_date;
    }
}
