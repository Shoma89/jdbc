package JDBC_Part2;

import JDBC_Part2.dao.DAOClientPerson;
import JDBC_Part2.model.ClientPerson;
import JDBC_Part2.model.Credit;
import JDBC_Part2.model.Deposits;
import JDBC_Part2.service.ClientPersonService;
import JDBC_Part2.service.CreditService;
import JDBC_Part2.service.DepositsService;
import org.apache.logging.log4j.core.util.JsonUtils;

public class TestBank {
    public static void main(String[] args) {
        CreditService creditService = new CreditService();
        DepositsService depositsService = new DepositsService();
        DAOClientPerson daoClientPerson = new DAOClientPerson();
        ClientPerson clientPerson = new ClientPerson("Donald", "Trump", 89273569245L, "05/09/1942");
        Credit credit1 = new Credit("Ipoteka", 18, 800_000L, 11);
        Deposits deposits = new Deposits("Dolgochniy", 120, 20_000_000L, 15);

        ClientPersonService personService = new ClientPersonService();
        depositsService.deleteDeposits(3);
    }
}
