package SimpleVersion;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Setter
@Getter

public class Person {
    private int id;
    private String name;
    private String last_name;
    private int age;
    private double salary;
}
