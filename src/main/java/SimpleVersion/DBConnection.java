package SimpleVersion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

public class DBConnection {
    public static Statement createStatement(){
        Statement statement;
        try {
            Properties properties = new Properties();
            FileInputStream fileInputStream = new FileInputStream("src/main/java/SimpleVersion/db.properties");
            properties.load(fileInputStream);
            Connection connection = DriverManager.getConnection(
                    properties.getProperty("db.url"),
                    properties.getProperty("db.user"),
                    properties.getProperty("db.password")
            );
            statement = connection.createStatement();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return statement;
    }
}
