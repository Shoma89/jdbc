package SimpleVersion;

import JDBCMain.model.Car;

import java.sql.*;
import java.util.ArrayList;

public class Main {
//    public static void main(String[] args) {
//        try {
//            ArrayList<Car> list = new ArrayList<>();
//            //интерфейс, который позволяет подключиться к бд
//            Connection connection = DriverManager.getConnection(
//                    "jdbc:postgresql://localhost:5432/Lesson_sql",
//                    "postgres",
//                    "kS%_334B"
//            );
//            //интерфейс, через который мы будем отправлять запросы
//            Statement statement = connection.createStatement();
//            //ResultSet хранит в себе результат запроса в бд
//            ResultSet resultSet = statement.executeQuery("SELECT * FROM car");
//            while (resultSet.next()){
//                list.add(new Car(resultSet.getInt("id"),
//                        resultSet.getString("brand"),
//                        resultSet.getString("model"),
//                        resultSet.getString("color"),
//                        resultSet.getInt("driver_id")));
//            }
//            for(Car car : list){
//                System.out.println(car);
//            }
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public static void main(String[] args) {

        try {
            Statement statement = DBConnection.createStatement();
            ArrayList<Car> list = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM car");
            while (resultSet.next()){
                list.add(new Car(resultSet.getInt("id"),
                        resultSet.getString("brand"),
                        resultSet.getString("model"),
                        resultSet.getString("color"),
                        resultSet.getInt("driver_id")));
            }
            for(Car car : list){
                System.out.println(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
