package JDBCMain.model;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class Car {
    private int id;
    private String brand;
    private String model;
    private String color;
    private int driverId;

    public Car(String brand, String model, String color, int driverId) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.driverId = driverId;
    }
}
