package JDBCMain.model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class User {
    private int id;
    private String first_name;
    private String last_name;
    private String birth_date;
    private String phone_number;

    public User(String first_name, String last_name, String birth_date, String phone_number) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
        this.phone_number = phone_number;
    }
}
