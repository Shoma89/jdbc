package JDBCMain.dao;

import JDBCMain.model.Car;
import JDBCMain.util.CarMapper;
import JDBCMain.util.DBUtil;
import SimpleVersion.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CarDAO {
    public Car findCarById(int id){
        CarMapper carMapper = new CarMapper();
        Car car = null;
        try(PreparedStatement pt = DBUtil.creatConnect().prepareStatement("select * from car where id = " + id);
            ResultSet resultSet = pt.executeQuery()){
            resultSet.next();
            car = carMapper.createCar(resultSet);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return car;
    }

    public boolean insertCar(Car car, int driver_id) {
        boolean isSuccess = false;
        try(Statement statement = DBConnection.createStatement()){
            isSuccess =  statement
                    .execute("INSERT INTO car (id, brand, model, color, driver_id) " +
                            "VALUES ('" + car.getId() + "', '" + car.getBrand() + "', '" + car.getModel() + "', '" +
                            car.getColor() + "', '" + driver_id + "')");
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isSuccess;
    }
}
