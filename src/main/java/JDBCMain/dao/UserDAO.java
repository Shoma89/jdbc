package JDBCMain.dao;

import JDBCMain.model.Car;
import JDBCMain.model.User;
import JDBCMain.util.CarMapper;
import JDBCMain.util.DBUtil;
import JDBCMain.util.UserMapper;
import SimpleVersion.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {
    public User findUserById(int id){
        UserMapper userMapper = new UserMapper();
        User user = null;
        try(PreparedStatement pt = DBUtil.creatConnect().prepareStatement("select * from users where id = " + id);
            ResultSet resultSet = pt.executeQuery()){
            resultSet.next();
            user = userMapper.createUser(resultSet);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return user;
    }

    public boolean insertUser(User user) {
        boolean isSuccess = false;
        try(Statement statement = DBConnection.createStatement()) {
            String s = ("INSERT INTO users(id, first_name, last_name, birth_date, phone_number) " + "VALUES ('" + user.getId()
            + "', '" + user.getFirst_name() + "', '" + user.getLast_name() + "', '" + user.getBirth_date() + "', '" + user.getPhone_number() + "')");
            isSuccess = statement.execute(s);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return isSuccess;
    }
}
