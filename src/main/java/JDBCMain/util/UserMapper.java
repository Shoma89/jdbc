package JDBCMain.util;

import JDBCMain.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
    public User createUser(ResultSet resultSet) throws SQLException {
        User user = new User(
                resultSet.getInt("id"),
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                resultSet.getString("birth_date"),
                resultSet.getString("phone_number"));
        return user;
    }
}
