package JDBCMain.util;

import JDBCMain.model.Car;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarMapper {
    public Car createCar(ResultSet resultSet) throws SQLException {
        Car car = new Car(resultSet.getInt("id"),
                resultSet.getString("brand"),
                resultSet.getString("model"),
                resultSet.getString("color"),
                resultSet.getInt("driver_id"));
        return car;
    }
}
