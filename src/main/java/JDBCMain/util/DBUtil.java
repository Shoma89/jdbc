package JDBCMain.util;

import lombok.AllArgsConstructor;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DBUtil {
    private static Connection connection;

    public DBUtil(Connection connection) {
        this.connection = creatConnect();
    }

    public static Connection creatConnect(){
        if(connection == null){
            Properties properties = new Properties();

            try(FileInputStream fileInputStream = new FileInputStream("src/main/java/SimpleVersion/db.properties")) {
                properties.load(fileInputStream);
                return DriverManager.getConnection(
                        properties.getProperty("db.url"),
                        properties.getProperty("db.user"),
                        properties.getProperty("db.password")
                );
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return  connection;
    }
}
