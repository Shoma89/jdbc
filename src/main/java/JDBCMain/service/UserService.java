package JDBCMain.service;

import JDBCMain.dao.CarDAO;
import JDBCMain.dao.UserDAO;
import JDBCMain.model.Car;
import JDBCMain.model.User;

public class UserService {
    UserDAO userDAO = new UserDAO();
    CarDAO carDAO = new CarDAO();
    public User findUserById(int id){
        return userDAO.findUserById(id);
    }
    public boolean purchaseCar(User user, Car car){
        boolean userB = userDAO.insertUser(user);
        boolean carB = carDAO.insertCar(car, user.getId());
        return userB && carB;
    }
}
