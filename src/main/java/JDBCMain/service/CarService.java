package JDBCMain.service;

import JDBCMain.dao.CarDAO;
import JDBCMain.model.Car;

public class CarService {
    CarDAO carDAO = new CarDAO();
    UserService userService = new UserService();
    public Car findCarById(int id){
        Car car = carDAO.findCarById(id);
        System.out.println(userService.findUserById(car.getDriverId()));
        return car;
    }
}
