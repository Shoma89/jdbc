package pro;

import JDBCMain.util.DBUtil;
import SimpleVersion.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static final String FIND_ALL = "SELECT * FROM ";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try(Statement statement = DBUtil.creatConnect().createStatement();
            ResultSet resultSet = statement.executeQuery(scanner.nextLine())) {
            ArrayList<Person> personArrayList = new ArrayList<>();
//            Car car = new Car(resultSet.getInt("id"),
//                    resultSet.getString("brand"),
//                    resultSet.getString("model"),
//                    resultSet.getString("color"),
//                    resultSet.getInt("driver_id"));
            while (resultSet.next()) {
                personArrayList.add(new Person(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("last_name"),
                        resultSet.getInt("age"),
                        resultSet.getDouble("salary")));
            }
            System.out.println(personArrayList.size());
            System.out.println(personArrayList.get(1));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
