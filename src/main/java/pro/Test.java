package pro;

import JDBCMain.model.User;
import JDBCMain.util.DBUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        ArrayList<User> arrayList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        try(PreparedStatement preparedStatement = DBUtil.creatConnect().prepareStatement(scanner.nextLine());
            ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()){
                arrayList.add(new User(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("birth_date"),
                        resultSet.getString("phone_number")));
            }
            for(User user : arrayList){
                System.out.println(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
